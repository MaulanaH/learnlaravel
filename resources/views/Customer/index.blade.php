@extends('layouts.sb-admin')

@section('home')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Customer</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        
                    <div class="panel panel-default">
                        <div class="panel-heading">
                         Tabel Customer
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                	
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Alamat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($customer as $customer_list)
                                        <tr>
                                            <td>{{$customer_list->customer_id}}</td>
                                            <td>{{$customer_list->name}}</td>
                                            <td>{{$customer_list->addres}}</td>
                                            <td><a href="/Customer/{{$customer_list->customer_id}}">Show</a></td>
                                            <td><a href="/Customer/{{$customer_list->customer_id}}/edit">Edit</a></td>
                                            <td><form action="/Customer/{{$customer_list->customer_id}}" method="post">{{ csrf_field() }} {{ method_field('DELETE') }}<button>X</button></form></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    
                                </table>
                                <h3><a href="/Customer/create">Create Data</a></h3>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

    </div>
@endsection