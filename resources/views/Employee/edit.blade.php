@extends('layouts.sb-admin')

@section('home')

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Employe
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form">
                                        @foreach ($employee as $employee_list)
											<div class="form-group">
												<label>Id</label>
													<input class="form-control" value="{{ $employee_list->employee_id }}"> <br>
												<label>Nama</label>
													<input class="form-control" value="{{ $employee_list->employee_name }}"> <br>
												<label>Alamat</label>
													<input class="form-control" value="{{ $employee_list->employee_address }}"> <br>
												<label>Nomor Telp.</label>
													<input class="form-control" value="{{ $employee_list->employee_phone_number }}"> <br>

												<button type="submit">Submit</button> <br>
											</div>
										@endforeach
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

@endsection