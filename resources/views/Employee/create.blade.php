@extends('layouts.sb-admin')

@section('home')

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Form Tambah Data</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Data Employe
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form"  action="/Employee" method="post">
                                        {{ csrf_field() }}
											<div class="form-group">
													<label for="txt_id">Id</label>
													<input type="text" name="txt_id" id="txt_id" class="form-control"> <br>
													<label for="txt_name">Nama</label>
													<input type="text" name="txt_name" class="form-control"> <br>
													<label for="txt_address">Alamat</label>
													<input type="text" name="txt_address" class="form-control"> <br>
													<label for="txt_phone">Nomor HP</label>
													<input type="text" name="txt_phone" class="form-control"> <br>

													
													<input type="submit" name="sbm_save" id="sbm_save" value="Simpan">

													@foreach ($errors->all() as $error)
													<div>{{ $error }}</div>
													@endforeach

													
											</div>
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->

@endsection