@extends('layouts.sb-admin')

@section('home')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Employee</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        
                    <div class="panel panel-default">
                        <div class="panel-heading">
                         Tabel Employee
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table">
                                	
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Alamat</th>
                                            <th>No. Telp</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach ($employee as $employee_list)
                                        <tr>
                                            <td>{{$employee_list->employee_id}}</td>
                                            <td>{{$employee_list->employee_name}}</td>
                                            <td>{{$employee_list->employee_address}}</td>
                                            <td>{{$employee_list->employee_phone_number}}</td>
                                            
                                            <td>
                                            	<a href="/Employee/{{$employee_list->employee_id}}">
                                            	<button type="button" class="btn btn-primary">
                                            		Show
                                            	</button>
                                            	</a>
                                            </td>

                                            <td>
                                            	<a href="/Employee/{{$employee_list->employee_id}}/edit">
                                           		<button type="button" class="btn btn-info">
                                            		Edit
                                            	</button>
                                        		</a>
                                        	</td>

                                            <td><form action="/Employee/{{$employee_list->employee_id}}" method="post">
                                            {{ csrf_field() }} {{ method_field('DELETE') }}
                                            <button class="btn btn-danger">Delete</button></form></td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    
                                </table>
                                <h3><a href="/Employee/create">Create Data</a></h3>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->

    </div>
@endsection