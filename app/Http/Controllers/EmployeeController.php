<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Validator;

class EmployeeController extends Controller
{
    function index(){

    	$employee = Employee::get(['employee_id', 
    		'employee_name', 
    		'employee_address',
    		'employee_phone_number']);

    	
    	return view('employee.index', compact('employee'));
    }


    public function __construct()
    {
        $this->middleware('auth');
    }



    function create(){
    	return view('employee.create');
    }


    function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'txt_id' => 'required|string|max:12',
            'txt_name' => 'required|string|max:100',
            'txt_address' => 'required|string|max:225',
            'txt_phone' => 'required|string|max:13',

        ])->validate();

    	$txtId = $request->input('txt_id');
    	$txtName = $request->input('txt_name');
    	$txtAddress = $request->input('txt_address');
        $txtPhone = $request->input('txt_phone');


    	Employee::create([
    		'employee_id' => $txtId, 
    		'employee_name' => $txtName, 
    		'employee_address' => $txtAddress,
    		'employee_phone_number' => $txtPhone
    	]);

        
    	return redirect('/Employee');
    }

    public function show($id) {

        $employee = Employee::where('Employee_id', $id)->get();
        return view('employee.show', compact('employee'));
    }

    public function edit($id) {

        $employee = Employee::where('employee_id', $id)->get();
        return view('employee.edit', compact('employee'));
    }

    public function update(Request $request, $id) {
        
        $txtId = $request->input('txt_id');
        $txtName = $request->input('txt_name');
        $txtAddress = $request->input('txt_address');
        $txtPhone = $request->input('txt_phone');

        Employee::where('employee_id', $id)->update([
            'employee_name' => $txtName,
            'employee_address' => $txtAddress,
            'employee_phone_number' => $txtPhone
        ]);

        return redirect('/Employee');
    }

    public function destroy($id){
        $employee = Employee::where('employee_id', $id)->first();
        $employee->delete();
        return redirect('/Employee');
    }
}
